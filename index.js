// Directions:
// Create two event listeners for when a user types in the first and last name inputs
// When this event triggers, update the span-full-name's content to show the value of the first name input on the left and the value of the last name input on the right

// Stretch goal: Instead of an anonymous function, create a new function that the two event listeners will call.

// Guide question: Where do the names come from and where should they go?


let firstName = document.querySelector('#txt-first-name');
let lastName = document.querySelector('#txt-last-name');
let fullName = document.querySelector('#span-full-name');

function concatAndUpdateInputs() {
	fullName.textContent = firstName.value + ' ' + lastName.value;
}

firstName.addEventListener('input', () => {
	concatAndUpdateInputs();
});

lastName.addEventListener('input', () => {
	concatAndUpdateInputs();
});

